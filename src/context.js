/* eslint-disable */

import React, { Component } from 'react'
import axios from 'axios'
import Loader from '../src/components/Spinner/Spinner'
import spinner from '../src/components/Spinner/Spinner';

const ProductContext = React.createContext()

export default class ProductProvider extends Component {
  constructor(props) {
    super(props)
    this.state = {
      error: null,
      loading: true,
      items: [],
      selectedItem: {},
      getItem: this.getItem,
      cart: [],
      addToCart: this.addToCart,
      cartSubTotal: 0,
      cartTax: 0,
      cartTotal: 0,
      ratings_only_review_count: 0
    }
  }
 

  componentDidMount() {
    axios.get('https://hpwd-ecom-api.herokuapp.com/api/v1/items').then(res => {
      // console.log(res.data, 'this is my res from context')
      this.setState({
        loading: false,
        items: res.data
      })
    })
  }

  getItem = tcin => {
    const items = this.state.items.find(item => item.tcin === tcin)
    return items
  }

  // handleDetail = tcin => {
  //   const item = this.getItem(tcin)
  //   this.setState({
  //     selectedItem: item
  //   })
  // }

  addToCart = tcin => {
    let tempProducts = [...this.state.items]
    const index = tempProducts.indexOf(this.getItem(tcin))
    let product = tempProducts[index]
    product.ratings_only_review_count = 0
    product.ratings_only_review_count = product.ratings_only_review_count + 1 //count
    const price = product.price.current_retail_max

    this.setState(
      () => {
        return { items: tempProducts, cart: [...this.state.cart, product] }
      },
      () => {
        this.addTotals()
      }
    )
  }
  increment = tcin => {
    let tempCart = [...this.state.cart]
    const selectedItem = tempCart.find(item => item.tcin == tcin)
    const index = tempCart.indexOf(selectedItem)
    const item = tempCart[index]

    item.ratings_only_review_count = item.ratings_only_review_count + 1 // item.ratings_only_review_count = count of an item
   
    item.price.current_retail_max =
      item.ratings_only_review_count * item.price.current_retail_min // item.price.current_retail_max = total
    
    this.setState(
      () => {
        return { cart: [...tempCart] }
      },
      () => {
        this.addTotals()
      }
    )
  }
  decrement = tcin => {
    let tempCart = [...this.state.cart]
    const selectedItem = tempCart.find(item => item.tcin == tcin)
    const index = tempCart.indexOf(selectedItem)
    const item = tempCart[index]

    item.ratings_only_review_count = item.ratings_only_review_count - 1 // item.ratings_only_review_count = count of an item
    if (item.ratings_only_review_count === 0) {
      this.removeItem(tcin)
    } else {
      item.price.current_retail_max =
        item.ratings_only_review_count * item.price.current_retail_min // item.price.current_retail_max = total

      this.setState(
        () => {
          return { cart: [...tempCart] }
        },
        () => {
          this.addTotals()
        }
      )
    }
  }
  removeItem = tcin => {
    let tempProducts = [...this.state.items]
    let tempCart = [...this.state.cart]

    tempCart = tempCart.filter(item => item.tcin !== tcin)

    const index = tempProducts.indexOf(this.getItem(tcin))
    let removedProduct = tempProducts[index]
    removedProduct.total_reviews = 0

    this.setState(
      () => {
        return {
          cart: [...tempCart],
          items: [...tempProducts]
        }
      },
      () => {
        this.addTotals()
      }
    )
  }
  clearCart = id => {
    this.setState(() => {
      return { cart: [] }
    })
  }
  addTotals = () => {
    let tempSubTotal = 0
    this.state.cart.map(item => (tempSubTotal += item.price.current_retail_max))
    let subTotal = parseFloat(tempSubTotal.toFixed(2))
    const tempTax = subTotal * 0.05
    const tax = parseFloat(tempTax.toFixed(2))
    const tempTotal = subTotal + tax
    const total = parseFloat(tempTotal.toFixed(2))

    this.setState(() => {
      return {
        cartSubTotal: subTotal,
        cartTax: tax,
        cartTotal: total
      }
    })
  }
  render() {
    return (
      <ProductContext.Provider
        value={{
          ...this.state,
          addToCart: this.addToCart,
          increment: this.increment,
          decrement: this.decrement,
          removeItem: this.removeItem,
          clearCart: this.clearCart
        }}
      >
        {this.props.children}
      </ProductContext.Provider>
    )
  }
}

const ProductConsumer = ProductContext.Consumer

export { ProductProvider, ProductConsumer }
