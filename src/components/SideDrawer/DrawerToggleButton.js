/* eslint-disable */

import React from 'react'
import './DrawerToggleButton.css'

const drawerToggleButton = () => (
  <button className="toggle-button">
    <div className="toggle-button__line" />
    <div className="toggle-button__line" />
    <div className="toggle-button__line" />
  </button>
)
export default drawerToggleButton
