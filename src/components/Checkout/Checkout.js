import React, { Component } from 'react'
import './Checkout.css'
import { Link } from 'react-router-dom'
import { ProductConsumer } from '../../context'
import CheckoutList from '../Checkout/CheckoutList'
 import paypal from '../../images/paypal-logo-preview.png'
 import visa from '../../images/Visa_2014_logo_detail.png'
 import masterCard from '../../images/1200px-Mastercard-logo.png'
 import Master from '../../images/1200px-Maestro_2016.png'
 import Discover from '../../images/1280px-Discover_Card_logo.png'
 import Deal from '../../images/ideal-logo-png-transparent.png'
 import Dhl from '../../images/DHL_Logo.png'
 import Dpd from '../../images/dpd.png'
 import Fedex from '../../images/fedex.png'
 import InPost from '../../images/image0.png'

class Checkout extends Component {
  render() {
    return (
      <ProductConsumer>
        {value => {
           console.log(value, 'changed again')
           const { cartTotal } = value
          console.log(cartTotal, ' cart from cc')
          return (
            <div>
              <div className="checkOutGrid">
                <div>
                  <h2 className="header">Shipping and Payment</h2>
                  <button className="buttonCheckout">LOG IN</button>
                  <button className="buttonCheckoutTwo">SIGN UP</button>
                  <div>
                    <h2 className="header">Shipping Information</h2>
                  </div>
                  <div className="inputTop">
                    <input
                      required
                      className="inputGrid"
                      type="text"
                      placeholder="Email"
                    ></input>
                    <input
                    required
                      className="inputGrid"
                      type="text"
                      placeholder="Address"
                    ></input>
                    <input
                    required
                      className="inputGrid"
                      type="text"
                      placeholder="First name"
                    ></input>
                    <input
                    required
                      className="inputGrid"
                      type="text"
                      placeholder="Address Line 2"
                    ></input>
                    <input
                    required
                      className="inputGrid"
                      type="text"
                      placeholder="Last name"
                    ></input>
                    <input
                    required
                      className="inputGrid"
                      type="text"
                      placeholder="City"
                    ></input>
                    <input
                    required
                      className="inputGrid"
                      type="text"
                      placeholder="Phone Number"
                    ></input>
                    <input
                    required
                      className="inputGrid"
                      type="text"
                      placeholder="Postal Code / Zip"
                    ></input>
                  </div>
                </div>
                <div>
                  <div>
                    <h2 className="header">Payment method</h2>
                  </div>
                  <div className="paymentTop">
                    <button className="paymentButton"><img src={paypal} ></img></button>
                    <button className="paymentButton"><img src={visa} ></img></button>
                    <button className="paymentButton"><img src={masterCard} ></img></button>
                    <button className="paymentButton"><img src={Master} ></img></button>
                    <button className="paymentButton"><img src={Discover} ></img></button>
                    <button className="paymentButton"><img src={Deal} ></img></button>
                  </div>

                  <br></br>
                  <br></br>
                  <div>
                    <h2 className="header">Delivery method</h2>
                  </div>
                  <div className="deliveruMethod">
                    <button className="paymentButtonTwo"><img src={Dhl} ></img> $20.00</button>
                    <button className="paymentButtonTwo"><img src={Dpd} ></img> $12.00</button>
                    <button className="paymentButtonTwo"><img src={InPost} ></img> $15.00</button>
                    <button className="paymentButtonTwo"><img src={Fedex} ></img> $10.00</button>
                  </div>
                </div>
                <div>
                  <div><h2 className="header">Your Cart</h2></div>
                  <React.Fragment>
                    <CheckoutList value={{ value }} />
                    <button className="cartTotalButton"><div>Your Total is: {cartTotal} </div></button>
                  </React.Fragment>
                </div>
              </div>
              <div className="buttonGrid">
                <Link to="/cart">
                  <button className="checkoutButton">Go Back </button>
                </Link>
                <Link to="/">
                  <button className="checkoutButton">Continue shopping </button>
                </Link>
                <Link to="/orderCompleted">
                  <button className="checkoutButton">proceed payment </button>
                </Link>
              </div>
            </div>
          )
        }}
      </ProductConsumer>
    )
  }
}

export default Checkout
