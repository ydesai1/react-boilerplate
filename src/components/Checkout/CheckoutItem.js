import React from 'react'
// import './Cart.css';
import './Checkout.css'


const CheckoutItem = ({ item, value }) => {
  const { tcin, image, price, ratings_only_review_count, title, count } = item
  
  const { cartTotal } = value
  return (
      <React.Fragment>
        <div className="CheckoutItemGrid">
          <div><img src={image} style={{ width: '5rem', height: '5rem', borderRadius: "50%" }} /></div>
          <div><span>Product: </span> {title}</div>
          <div><strong> {price.current_retail_max.toFixed(2)}</strong></div>
        </div>
      </React.Fragment>
       
     
  )
}
export default CheckoutItem
