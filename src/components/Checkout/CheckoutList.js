import React from 'react'
import CheckoutItem from './CheckoutItem'
function CheckoutList({ value }) {
  const { cart } = value.value
  return (
    <div>
      {cart.map(item => {
        return <CheckoutItem key={item.tcin} item={item} value={value} />
      })}
    </div>
  )
}
export default CheckoutList
