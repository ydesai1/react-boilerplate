/* eslint-disable */

import React, { Component } from 'react'
import Product from './Product'
import Title from './Title'
import { ProductConsumer } from '../context'
import Spinner from '../components/Spinner/Spinner'

class ProductList extends Component {
  
  render() {
    
    return (
      <React.Fragment>
        {/* <Title name="our" title="product" /> */}
        <div>
          <div className="container">
            <ProductConsumer>
              {value => {
                // let loader = <Spinner />
                   console.log(value.loading, 'this is value')
                  if(value.loading == false) {
                    return value.items.map(item => {
                      return <Product key={item.tcin} item={item} value={value} />
                    })
                  }
                  return (
                    <Spinner>
                      <p>I am loading</p>
                    </Spinner>
                    
                  )
                
              }}
            </ProductConsumer>
          </div>
        </div>
      </React.Fragment>

      // <Product />
    )
  }
}

export default ProductList
