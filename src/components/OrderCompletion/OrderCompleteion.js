import React from 'react';
import {Link} from 'react-router-dom';
import Completed from '../../images/completedtwo.png'
import './OrderCompletion.css'

const OrderCompletion = () => {
  return (
    <div>
      <div className="completedContainer">
        <div ><img className="completedImage" src={Completed} ></img></div>
        <div className="messageMargin">Order Completed Successfully</div>
        <div><Link to="/">
              <button className="emptyCartButton">Go to Home Page</button>
            </Link>
        </div>
      </div>
    </div>
  )
}

export default OrderCompletion
