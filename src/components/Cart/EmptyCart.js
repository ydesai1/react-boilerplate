import React from 'react';
import './EmptyCart.css';
import {Link} from 'react-router-dom';

const EmptyCart = () => {
  return (
    <div>
      <div className="emptyCart">
        <div className="cartMarginTwo">Shopping Cart</div>
        <div className="cartMargin">Your Cart is Empty</div>
        <div><Link to="/">
              <button className="emptyCartButton">Go to Home Page</button>
            </Link>
        </div>
      </div>
    </div>
  )
}

export default EmptyCart
