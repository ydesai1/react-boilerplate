import React from 'react'
import { Link } from 'react-router-dom'
import './CartTotals.css'

const CartTotals = ({ value }) => {
  const { cartSubTotal, cartTax, cartTotal, clearCart } = value
  const checkout = () => {
    
  }
  return (
    <React.Fragment>
      
          <div className="cartTotalGrid">
            <Link to="/">
              {/* <button type="button" onClick={() => clearCart()}> */}
              &#8592;
              <button type="button" className="backButton">
                 <h1>Continue Shopping</h1>
              </button>
            </Link>
            {/* <p>
              <span >subtotal:</span><span style={{fontWeight:"bold"}}> ${cartSubTotal}</span>
            </p>
            <p>
              <span>cartTax: ${cartTax}</span>
            </p> */}
            <p>
              <span>Total Cost: $ {cartTotal}</span>
            </p>
            <Link to="/checkout">
              <button onClick={() => checkout()} className="checkoutButton">
                Checkout
              </button>
            </Link>
          </div>
        
    </React.Fragment>
  )
}
export default CartTotals
