import React, { Component } from 'react'
import { ProductConsumer } from '../../context'
import Title from '../Title'
import CartColumns from './CartColumns'
import EmptyCart from './EmptyCart'
import CartList from './CartList'
import CartTotals from './CartTotals'
import Checkout from '../Checkout/Checkout'

class Cart extends Component {
  render() {
    return (
      <section>
        <ProductConsumer>
          {value => {
             console.log(value, 'value in cart')
            const { cart } = value
            // const { size } = this.props.location.state
            if (cart.length > 0) {
              return (
                <React.Fragment>
                  <Title name="Shopping" title="cart" />
                  <CartColumns />
                  <CartList value={{ value}} />
                  <CartTotals value={value} />
                  {/* <br></br>
                  <br></br>
                  <br></br>
                  <Checkout /> */}
                </React.Fragment>
              )
            } else {
              return <EmptyCart />
            }
          }}
        </ProductConsumer>
      </section>
    )
  }
}

export default Cart
