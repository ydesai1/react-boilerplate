import React from 'react'
import './CartItem.css'

const CartItem = ({ item, value }) => {
  const { tcin, image, price, ratings_only_review_count, title, count } = item
  const { increment, decrement, removeItem } = value.value
   console.log(item, value)
 
  return (
    <div className="mainCartGrid">
      <div className="cartGridItem">
        <div className="titleWrapper">
          <div>
            <img src={image} style={{ width: '5rem', height: '5rem', borderRadius: "50%" }} />
          </div>
          <div>{title}</div>
        </div>

        {/* <div className="titleWrapper">
          <span>White</span>
        </div>
        <div className="titleWrapper">
          <span>{value.size}</span>
        </div> */}
        <div>
          <div>
            <div>
              <button className="buttonLayer">
              <button className="buttonCart" onClick={() => decrement(tcin)}>-</button>
              <button className="buttonCart">{ratings_only_review_count}</button>
              <button className="buttonCart" onClick={() => increment(tcin)}>+</button>
              </button>
            </div>
          </div>
        </div>
        <div className="titleWrapper">
          <strong> {price.current_retail_max.toFixed(2)}</strong>
        </div>
        <div >
          <button className="buttonBorder" onClick={() => removeItem(tcin)}>
            <strong> X </strong>
          </button>
        </div>
      </div>
    </div>
  )
}
export default CartItem
