import React, { Component } from 'react'
import { ProductConsumer } from '../context'
import { Link } from 'react-router-dom'
import './Details.css'
import { Title } from '../components/Title'
import { Swatches } from './Swatches'
import Loader from '../components/Spinner/Spinner'

class Details extends Component {
  
  state = {
    size : '',
    updateSize : this.updateSize
  }
  updateSize = () => {
    this.setState( {
      size : 'xs'
    },console.log(this.state.size))
  }


  render() {
    
    return (
      <div className="gridDetails">
        <ProductConsumer>
          {value => {
            if (
              value.getItem(this.props.match.params.id) === undefined ||
              value.getItem(this.props.match.params.id).length === 0
            ) {
              return <Loader></Loader>
            } else {
              // console.log(value, 'value only')
               console.log(value.getItem(this.props.match.params.id), 'value from details')
              const {
                image,
                tcin,
                title,
                price,
                swatches,
                total_review_count
              } = value.getItem(this.props.match.params.id)
              console.log(swatches,'swatches')
              return (
                <div className="detailsGridTwo">
                  <div className="images">
                    {/* <img src={swatches.color[0].img_url} alt="product"></img> */}
                    <img src={image} alt="product"></img>
                  </div>
                  <div>
                    <div className="detailsGridFour">
                      <div className="detailsGridTitle"><h2>{title}</h2></div>
                      <div>{price.formatted_current_price}</div>
                      <div>{price.formatted_current_price}</div>
                    </div>
                    <div className="btg">
                      <div>
                        <button className="detailsButton" onClick={() => this.updateSize()}> XS </button>
                      </div>
                      <div>
                      <button className="detailsButton"> SM </button>
                      </div>
                      <div>
                      <button className="detailsButton"> M </button>
                      </div>
                      <div>
                      <button className="detailsButton"> LG </button>
                      </div>
                      <div>
                      <button className="detailsButton"> XL </button>
                      </div>
                      <div>
                      <button className="detailsButton"> XXL </button>
                      </div>
                      
                      {/* <button className="detailsButton"> m </button>
                      <button className="detailsButton"> lg </button>
                      <button className="detailsButton"> xl </button>
                      <button className="detailsButton"> xxl </button> */}
                    </div>
                    <div className="swatchesGrid">
                      {swatches ? swatches.color.map(swatch => {
                        
                        const changeImage = () => {
                          console.log(swatch.partNumber)
                        }
                            return (
                              <button className="buttonBorder" >
                                <img
                                onClick={() => changeImage()}
                                className="swatches"
                                src={swatch.swatch_url}
                                alt="product"
                              ></img>
                              </button>
                              
                            )
                          })
                        : null}
                    </div>

                    

                    <div className="quanittyGrid">
                      {/* <p>Quantity</p>
                    <button className="button" onClick={() => {value.increment()}}> + </button>
                    <button className="button" onClick={() => {value.decrement()}}> - </button> */}
                      <br></br>
                      <Link to="/cart">
                        <button
                          className="button"
                          onClick={() => {
                            value.addToCart(tcin)
                          }}
                        >
                          Add To Cart
                        </button>
                      </Link>

                      <Link to="/">
                        <button className="button">Back</button>
                      </Link>
                    </div>
                  </div>
                </div>
              )
            }
          }}
        </ProductConsumer>
      </div>
    )
  }
}

export default Details
