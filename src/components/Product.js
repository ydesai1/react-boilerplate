/* eslint-disable */

import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import '../App.css'
import PropTypes from 'prop-types'
import { ProductConsumer } from '../context'
import Spinner from '../components/Spinner/Spinner'

class Product extends Component {
  render() {
    const { title, image, price, tcin } = this.props.item
    return (
        <ProductConsumer>
          {value =>  (
                <Link to={`/details/${tcin}`}>
                <div className="images">
                  <img src={image} alt="product"></img>
                </div>
                <br></br>
                <div className="box2">
                  <p className="overflow-visible">{title}</p>
                </div>
                <br></br>
                <div className="box3">
                  <p className="price">{price.formatted_current_price}</p>
                </div>
                {/* <div><p ><i className="fas fa-cart-plus"></i>In Cart</p></div> */}
              </Link>
              )
            }
        </ProductConsumer>
    )
  }
}

Product.propTypes = {
  item: PropTypes.shape({
    title: PropTypes.string,
    image: PropTypes.string
  }).isRequired
}

export default Product
