/* eslint-disable */

import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { hot } from 'react-hot-loader'
import { BrowserRouter as Router } from 'react-router-dom'
import Navbar from './components/Navbar'
import ProductList from './components/ProductList'
import Details from './components/Details'
import Cart from './components/Cart/Cart'
import Default from './components/Default'
import Footer from './components/Footer'
import Checkout from './components/Checkout/Checkout'
import Loader from '../src/components/Spinner/Spinner'
import OrderCompleted from '../src/components/OrderCompletion/OrderCompleteion'

class App extends React.Component {
  render() {
    console.log('hello')
    return (
      <React.Fragment>
        {/* <Loader /> */}
        
        <Router>
          <Navbar />
          <main style={{ paddingTop: '64px' }}>
            <Switch>
              <Route exact path="/" component={ProductList} />
              <Route path="/details/:id" component={Details} />
              <Route path="/cart" component={Cart} />
              <Route path="/checkout" component={Checkout} />
              <Route path="/orderCompleted" component={OrderCompleted} />
              <Route component={Default} />
            </Switch>
          </main>
        </Router>
        <Footer></Footer>
      </React.Fragment>
    )
  }
}
// adding some comments

export default hot(module)(App)
